# Pelatihan React Native @ Pusilkom UI

> Pelatihan pengembangan aplikasi mobile lintas *platform* menggunakan
> *framework* React Native di Pusilkom UI dengan durasi selama 1 hari kerja.

Dokumen ini menjelaskan garis besar pelatihan pengembangan aplikasi mobile
lintas *platform* menggunakan *framework* React Native yang diadakan di
Pusilkom UI.

## Daftar Isi

1. [Daftar Isi](#daftar-isi)
2. [Deskripsi Pelatihan](#deskripsi-pelatihan)
3. [Rundown](#rundown)
4. [Silabus](#silabus)
5. [Struktur Proyek](#struktur-proyek)
6. [Changelog](#changelog)
7. [Instruktur](#instruktur)
8. [Lisensi & Hak Cipta](#lisensi-hak-cipta)

## Deskripsi Pelatihan

Pelatihan ini bertujuan untuk memberikan pengetahuan serta pengalaman kepada
peserta pelatihan dalam mengembangkan aplikasi *mobile* lintas *platform*
menggunakan *framework* React Native. Peserta pelatihan akan belajar dan
mencoba langsung membuat sebuah aplikasi sederhana dengan mengikuti sebuah
studi kasus dari produk nyata. Diharapkan pada akhir pelatihan, peserta
mengetahui secara umum alur pengembangan aplikasi *mobile* lintas *platform*
menggunakan *framework* React Native serta dapat menerapkannya sesuai dengan
kebutuhan.

## Rundown

| Jadwal        | Kegiatan                                           |
| :-----------: | :------------------------------------------------: |
| 08:00 - 08:30 | Intro: Cross-Platform App Development              |
| 08:30 - 09:30 | Hands-on: "Hello, World" React Native              |
| 09:30 - 10:00 | Coffee Break                                       |
| 10:00 - 10:30 | Hands-on: NativeBase UI Framework                  |
| 10:30 - 11:30 | Hands-on: QIR Conference App From Scratch (Bag. 1) |
| 11:30 - 13:30 | ISHOMA                                             |
| 13:30 - 15:00 | Hands-on: QIR Conference App From Scratch (Bag. 2) |
| 15:00 - 16:00 | Hands-on: Testing React Native App                 |

## Silabus

1. Pengenalan Pengembangan Aplikasi *Mobile* Lintas *Platform* dan *Framework*
   React Native
   - Kuliah Singkat: Motivasi Pengembangan Aplikasi Lintas *Platform*
2. Hands-on: "Hello, World" React Native
   - Persiapan *Toolchain* React Native
   - React Component Dasar
   - Props, State, dan Style
   - Pemanggilan Webservice
   - Transisi Antar Layar
3. Hands-on: NativeBase UI Framework
   - Update "Hello, World" dengan NativeBase
4. Hands-on: QIR Conference App From Scratch (Bag. 1)
   - Penjelasan Singkat *Requirements* Aplikasi
   - Implementasi Use Case: Daftar Berita dari RSS Feed
5. Hands-on: QIR Conference App From Scratch (Bag. 2)
   - Implementasi Use Case: Pelaporan via Helpdesk
6. Hands-on: Testing React Native App
   - Unit Testing menggunakan *Framework* `jest`
   - End-to-End Testing menggunakan Appium

## Struktur Proyek

Berikut ini adalah penjelasan susunan folder penyimpanan berkas-berkas terkait
dokumen instruksi pelatihan serta kode sumber aplikasi:

- `docs` -- Dokumen-dokumen instruksi pelatihan
- `code` -- Kode sumber aplikasi yang akan dikembangkan selama sesi pelatihan
  - `helloworld-base` -- *Template* awal aplikasi "Hello, World"
  - `helloworld-final` -- Hasil akhir aplikasi "Hello, World"
  - `qir-base` -- *Template* awal aplikasi QIR Conference
  - `qir-final` -- Hasil akhir aplikasi QIR Conference

## Changelog

Silakan baca dokumen [daftar perubahan (*changelog*)](CHANGELOG).

## Instruktur

- [Daya Adianto](https://gitlab.com/addianto)

## Lisensi & Hak Cipta

Copyright (c) 2019 Daya Adianto

Dokumen instruksi pelatihan serta kode sumber aplikasi dapat digunakan,
diubah, maupun dibagi (*share*) untuk keperluan lain selama mematuhi ketetapan
lisensi sumber terbuka (*open source*) yang berlaku. Kode sumber aplikasi
menggunakan lisensi [MIT](LICENSE), sedangkan dokumen instruksi pelatihan
menggunakan lisensi Creative Commons Attribution-ShareAlike 4.0 International
([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)).
