# Persiapan Toolchain

Agar dapat mengikuti instruksi-instruksi dalam pelatihan ini serta mulai
mengembangkan aplikasi React Native, ada beberapa *software* yang perlu
dipasang terlebih dahulu. Berikut ini adalah daftar *software* yang dibutuhkan:

- NodeJS v10.15.0 (LTS)
- npm v6.4.1
- Python v2.7.15
  > Catatan: Berdasarkan pengalaman instruktur, Python 3 juga dapa digunakan
  > walaupun tidak tercantum secara resmi dalam dokumentasi React Native.
- Java JDK v8+
- Android Studio v3.3.1

Setelah memasang *software*-*software* yang dibutuhkan, silakan baca instruksi
[Getting Started](https://facebook.github.io/react-native/docs/getting-started)
di situs dokumentasi React Native. Ikuti instruksi pada *tab* **Building Projects
with Native Code**.

> Catatan: Berdasarkan pengalaman instruktur, memang lebih mudah apabila
> mengikuti instruksi pada *tab* **Quick Start**. Namun proses untuk melakukan
> *build* ke *platform* Android/iOS akan lebih repot karena harus melepas
> (*eject*) *plugin* `expo` dari kode sumber aplikasi.

Ketika melakukan instalasi Android Studio, perhatikan juga kebutuhan-kebutuhan
tambahan yang harus dipasang melalui Android Studio. Pastikan untuk melakukan
instalasi Android SDK, SDK Platform, Performance, dan Android Virtual Device
(AVD). React Native versi 0.58.5 membutuhkan Android SDK versi Android Pie (SDK
Platform 28).

> Catatan: AVD adalah bagian dari *emulator* perangkat Android yang dapat
> digunakan untuk *testing* aplikasi Android tanpa harus memiliki perangkat
> Android. Apabila ingin menggunakan *emulator* Android di Windows 10, maka
> pastikan *hypervisor* (*engine* virtualisasi) Hyper-V telah dimatikan.
> *Emulator* Android di Windows saat ini hanya bisa berfungsi apabila dijalankan
> di *engine* virtualisasi VirtualBox.

Untuk memastikan semua *software* dan *library* telah terpasang dengan baik,
maka coba untuk membuat kerangka kode sumber aplikasi "Hello, World" sederhana.
Buka terminal/*shell* favorit kamu, lalu coba panggil perintah di dalam folder
`code`:

```bash
$ # Asumsi *current working directory* sedang berada di dalam folder `docs`
$ cd ../code
$ react-native init HelloWorld
$ cd HelloWorld
```

Setelah selesai memasang semua *software* dan *library* yang dibutuhkan, maka
coba nyalakan *emulator* Android/sambung perangkat Android ke PC, lalu panggil
perintah `react-native run-android` melalui *shell*. Jika kamu menggunakan
perangkat iOS, maka coba jalankan `react-native run-ios`. Apabila berhasil, maka
layar awal aplikasi React Native akan muncul di *emulator* atau perangkat tujuan.

![Tampilan awal aplikasi](images/react-native-awal-001.png)

## Checklist

- [ ] Telah memasang setiap *software* dan *library* yang dibutuhkan
  - NodeJS
  - Java JDK
  - Android Studio
- [ ] Telah mengatur *environment path* `ANDROID_HOME` dan `PATH` sesuai
  instruksi di laman [Getting Started](https://facebook.github.io/react-native/docs/getting-started)
- [ ] (Opsional) Telah memasang `yarn` sebagai alternatif *dependency manager* pada
  NodeJS
  > Catatan: Berdasarkan pengalaman instruktur, `yarn` lebih efisien ketika
  > mengunduh *library* dari repositori sentral *package* NodeJS. `npm` akan
  > selalu mengunduh dari awal satu atau lebih *package* walaupun mungkin
  > sudah pernah diunduh sebelumnya. `yarn` lebih pintar dalam hal ini dimana
  > `yarn` akan mencari *package* yang ingin diunduh di mesin lokal terlebih
  > dahulu.