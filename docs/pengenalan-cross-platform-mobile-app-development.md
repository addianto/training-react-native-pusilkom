# Pengenalan Cross Platform Mobile App Development Menggunakan React Native

> "Write Once, Run Anywhere" -- Sun Microsystems

Kutipan di atas merupakan kutipan populer yang mendasari pembuatan sebuah
bahasa pemrograman dan *platform* Java. Sebuah aplikasi dapat dituliskan
dalam bahasa Java dan hasil kompilasinya dapat dijalankan di berbagai *platform*
tanpa kompilasi ulang. Sebuah pengalaman yang cukup kontras apabila dibandingkan
dengan bahasa pemrograman lain seperti C yang harus dikompilasi ulang di atas
*platform* tujuan apabila berbeda dari *platform* pengembangan awal.

Secara tradisional, pengembangan aplikasi *mobile* harus mengikuti *platform*
dimana aplikasi tersebut akan operasional. Aplikasi untuk ponsel pintar Android
harus dituliskan dalam bahasa Java atau Kotlin dimana kode sumbernya akan
dikompilasi menggunakan SDK *platform* Android. Demikian juga untuk ponsel
pintar Apple. Aplikasi dituliskan dalam bahasa Objective-C atau Swift, lalu
dikompilasi di atas *platform* iOS. Sebuah hal yang lumrah dilakukan, namun
akan menjadi sulit ketika aplikasi *mobile* tersebut dibangun untuk berjalan
di lebih dari satu *platform*. Bayangkan banyaknya jam kerja yang harus
didedikasikan untuk mengembangkan sebuah aplikasi *mobile* untuk, misalnya,
dua buah *platform* dimana masing-masing *platform* menggunakan bahasa
pemrograman yang berbeda dan memiliki variasi API.

Cara kerja pengembangan aplikasi *mobile* yang dideskripsikan pada paragraf
sebelumnya disebut sebagai pengembangan aplikasi secara **native**. Aplikasi
dibangun spesifik untuk sebuah *platform*. Apabila aplikasi dengan kebutuhan
yang sama ingin dibangun juga untuk *platform* yang berbeda, maka tim
pengembang akan membuat *codebase* yang terpisah dari kode sumber aplikasi
yang asli dan berpotensi menyebabkan duplikasi pekerjaan.

Alternatif cara pengembangan aplikasi *mobile* yang sedang banyak dibicarakan
adalah pengembangan aplikasi *mobile* lintas *platform*. Pengembangan cukup
dilakukan dalam satu *platform* saja yang kemudian dapat dibangun ke *platform*
tujuan dengan mudah. Cara ini dipelopori oleh Apache Cordova (dahulu: PhoneGap)
dengan mengeksploitasi fakta bahwa setiap *platform* ponsel pintar pasti
memiliki komponen Web *browser* di dalamnya. Oleh karena itu, pengembang
aplikasi *mobile* cukup mengimplementasikan aplikasinya menggunakan teknologi
Web (HTML, CSS, JavaScript). Apache Cordova kemudian akan melakukan kompilasi
kode sumber aplikasi *mobile* rasa Web tersebut ke *platform* ponsel pintar
tujuan. Aplikasi akan operasional di *platform* tujuan di atas komponen Web
*browser* milik *platform*.

Walaupun pengembangan aplikasi *mobile* dengan teknologi Web memberikan
keuntungan di sisi kemudahan pembelajaran (misal: *skillset* pengembang
aplikasi Web bisa digunakan untuk membuat aplikasi *mobile*) dan efisiensi
kerja (misal: satu *codebase* untuk multi *platform*), masih ada aspek yang
masih membuat pengembangan aplikasi *mobile* secara *native* lebih diunggulkan.
Aplikasi *native* secara umum masih memiliki kinerja lebih cepat daripada
aplikasi *mobile* yang operasional di atas komponen Web *browser*. Selain
itu, aplikasi *native* dapat mengakses fitur-fitur yang spesifik dimiliki
sebuah *platform*.

Ada pendekatan lain yang berusaha memfasilitasi pengembangan aplikasi *mobile*
lintas *platform* dengan menggabungkan kemudahan teknologi Web dengan akses
fitur spesifik *platform* dan kinerja yang menyerupai aplikasi *native*, yaitu
dengan mengembangkan *virtual machine* atau *engine* yang berjalan di setiap
*platform* ponsel pintar. *Virtual machine* tersebut akan menerima *codebase*
yang dikembangkan menggunakan teknologi Web standar. Ketika aplikasi *mobile*
operasional di *platform* tujuan, maka aplikasi tersebut akan berjalan di
dalam *virtual machine* yang ikut diikutsertakan dengan aplikasi ketika
instalasi. Beberapa *framework* populer yang mengikuti pendekatan ini adalah
React Native, NativeBase, dan Xamarin.

Pada pelatihan ini, peserta akan diperkenalkan dengan *framework* React Native.
React Native adalah *framework* pengembangan aplikasi *mobile* yang menggunakan
JavaScript modern serta *library* React. Saat ini tidak ada alasan khusus
mengapa instruktur memilih *framework* ini sebagai bahan pelatihan selain
karena sudah pernah menggunakannya.